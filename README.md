# Windows 10 IoT Collection Target

Target module designed for the KAPE forensic program, developed by Eric Zimmerman. It facilitates the collection of relevant artifacts stored in the non-volatile memory of the Windows 10 IoT Core operating system.
Windows10IoTMainOS allows the collection from the "MainOS" partition of the filesystem.
Windows10IoTData allows the collection from the "Data" partition of the filesystem.